﻿namespace StupidFattyUpdater;

public enum RemotePointState : int // для битовых - множественне число 
{
    Undefined = 0,
    Allowed = 1,
    NotAllowed = 2
}

