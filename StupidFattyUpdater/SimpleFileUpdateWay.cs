﻿namespace StupidFattyUpdater;

public sealed class SimpleFileUpdateWay : UpdateWay
{
    List<RemotePoint> defaultRemotePoints;




    protected override RemotePointsBundle RemotePoints { get; set; }
  

    /* -------------------- */

    public override bool TryDownload()
    {
        //TODO
        return false;
    }

    public override AppVersion BiggestAppVersion { get; }

    /* -------------------- */

    SimpleFileUpdateWay()
    {
        defaultRemotePoints = new List<RemotePoint>();
        defaultRemotePoints.Add(new RemotePoint(@"Z:\ALL\temp\sfupdater"));
        defaultRemotePoints.Add(new RemotePoint(@"\\azr-files01\DATA\ALL\temp\sfupdater"));
        defaultRemotePoints.Add(new RemotePoint(@"L:\common\sfupdater"));
        defaultRemotePoints.Add(new RemotePoint(@"\\alphaone.file.core.windows.net\common\sfupdater"));
        defaultRemotePoints.Add(new RemotePoint(@"\\server-gis\sfupdater"));
    }


}
