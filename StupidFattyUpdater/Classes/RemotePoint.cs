﻿namespace StupidFattyUpdater;

public class RemotePoint //сделать референс? //структуру? //возможно запихнуть описание внутрь RemotePointsBundle?
{
    RemotePointState currentState;


    
    string remotePointPath;


    string remoteFilesHash;
    AppVersion lastRemoteVersion;


    protected RemotePointSpeed speed; //TODO скорость скачивания.
    bool isSecure;//TODO безопасность?

    /* -------------------- */

    public string RemotePointPath {
        get { return remotePointPath; }
        set { remotePointPath = value; }
    }


    public AppVersion LastRemoteVersion {
        get { return lastRemoteVersion; }
        set { lastRemoteVersion = value; }
    }


    /* -------------------- */

    public RemotePoint(string RemotePointPath)
    {
        currentState = RemotePointState.Undefined;
        lastRemoteVersion = new AppVersion { Major = -1, Minor = 0, Patch = 0 };
    }
}