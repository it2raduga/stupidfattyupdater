﻿namespace StupidFattyUpdater;

public class AppVersion
{
    int major;
    int minor;
    int patch;


    public int Major { init { major = value; } get { return major; } }
    public int Minor { init { minor = value; } get { return minor; } }
    public int Patch { init { patch = value; } get { return patch; } }


    public override string ToString()
    {
        return $"{Major}.{Minor}.{Patch}";
    }


    //TODO сравнение версий
}