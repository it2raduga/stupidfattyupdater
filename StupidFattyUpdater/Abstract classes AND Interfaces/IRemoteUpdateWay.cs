﻿namespace StupidFattyUpdater;

/*
    public interface SomeInterface
    {
        public void Public_m_from_intrfc();
        public abstract void Abstract_m_from_intrfc();
        protected abstract void ProtectedAbstract_m_from_intrfc();
    }
 */
public interface IRemoteUpdateWay
{
    /// <summary>
    /// Initiate download for first RemotePoint
    /// </summary>
    /// <returns>Returns false if download failde</returns>
    public bool TryDownload();
    public AppVersion BiggestAppVersion { get; }
}