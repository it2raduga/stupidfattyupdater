﻿namespace StupidFattyUpdater;

public abstract class UpdateWay : IRemoteUpdateWay
{
    public abstract bool TryDownload();

    public abstract AppVersion BiggestAppVersion { get;  }

    protected byte Priority { get; set; } //update way priority
    

    //TODO delegate for basic update mechanism
    // delegate ...

    //TODO delegate for RemotePointValidator. something like checker by pattern for specificUpdateWay derived from UpdateWay
    // delegate ...

    //TODO ЗАМЕР СКОРОСТИ??


    //TODO may be need readonly or something like readonly:
    protected abstract RemotePointsBundle RemotePoints { get; set; }

}
